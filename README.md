# Compression Ratio Calculator
This notebook provides the math necessary for calculating compression ratios for internal combustion piston engines.

It can be accessed at mybinder.org here: 
https://mybinder.org/v2/gl/burdickjp%2Fcompressionratiocalculator/master